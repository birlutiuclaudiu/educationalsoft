package model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Drawing implements java.io.Serializable{

    private List<GeometricElementDrawable> geometricElementDrawables;

    public Drawing(List<GeometricElementDrawable> geometricElementDrawables) {
        this.geometricElementDrawables = geometricElementDrawables;
    }

    public Drawing() {
        this.geometricElementDrawables = new ArrayList<>();
    }

    public List<GeometricElementDrawable> getGeometricElementDrawables() {
        return geometricElementDrawables;
    }

    public void setGeometricElementDrawables(List<GeometricElementDrawable> geometricElementDrawables) {
        this.geometricElementDrawables = geometricElementDrawables;
    }

    public void addGeometricElement(GeometricElementDrawable geometricElementDrawable) {
        this.geometricElementDrawables.add(geometricElementDrawable);
    }

    public void deleteGeometricElement(GeometricElementDrawable geometricElementDrawable) {
        this.geometricElementDrawables.remove(geometricElementDrawable);
    }

    public void drawGeometricElements(Graphics2D graphics2D) {
        for (GeometricElementDrawable geom : geometricElementDrawables) {
            geom.draw(graphics2D);
        }
    }
}
