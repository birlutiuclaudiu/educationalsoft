package view;

import presenter.Presenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.List;

public class EducationalSoftGUI extends JFrame implements IEduSoftView {
    private JPanel mainPanel;
    private JButton buttonColor;
    private JComboBox comboStyle;
    private JPanel resultsPanel;
    private JLabel arcLengthLabel;
    private JLabel sectionAriaLabel;
    private JLabel circleAriaLabel;
    private JLabel lengthCircleLabel;
    private JTextField widthTextField;
    private DrawingArea drawingArea;
    private JRadioButton drawCircleRadio;
    private JRadioButton drawPolygonRadio;
    private JComboBox propertiesTriangle;
    private JButton loadButton;
    private JButton saveButton;
    private JPanel drawingPanel;
    private JLabel circleRadiusLabel;
    private JTextField startArcAngle;
    private JTextField endArcAngle;
    private JTextField numberOfVerticesPolygonText;
    private JTextField numberOfVerticesText;
    private JRadioButton drawTriangleRadio;
    private ButtonGroup buttonGroupRadio;
    //pentru parametri
    private Color color = Color.BLACK;
    //attribute for drawing components
    private int oldX;
    private int oldY;
    private int newX;
    private int newY;

    //triangle coordinates
    private List<Float> triangleX;
    private List<Float> triangleY;

    public EducationalSoftGUI(String title) {
        super(title);
        drawingArea.setBound(this);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(mainPanel);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        // width will store the width of the screen
        int width = (int) (((float) 75 / 100) * size.getWidth());
        // height will store the height of the screen
        int height = (int) (((float) 75 / 100) * size.getHeight());
        resultsPanel.setMaximumSize(new Dimension(50, 50));
        resultsPanel.setPreferredSize(new Dimension(50, 50));
        resultsPanel.setBackground(Color.RED);
        buttonGroupRadio = new ButtonGroup();
        buttonGroupRadio.add(drawCircleRadio);
        buttonGroupRadio.add(drawPolygonRadio);
        buttonGroupRadio.add(drawTriangleRadio);
        drawCircleRadio.setActionCommand("circle");
        drawPolygonRadio.setActionCommand("polygon");
        drawTriangleRadio.setActionCommand("triangle");
        drawCircleRadio.setSelected(true);
        this.setMinimumSize(new Dimension(width, height));
        this.setLocationRelativeTo(null);
        this.pack();
        buttonColor.addActionListener(new ColorButtonListener());
        saveButton.addActionListener(new LoadSaveListener());
        loadButton.addActionListener(new LoadSaveListener());

        //update initial triangle
        triangleX = new ArrayList<>();
        triangleY = new ArrayList<>();
        triangleX.add(200f);
        triangleX.add(500f);
        triangleX.add(400f);
        triangleY.add(400f);
        triangleY.add(100f);
        triangleY.add(400f);
    }

    private class ColorButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Color initialColor = Color.BLACK;
            Color color = JColorChooser.showDialog(EducationalSoftGUI.this,
                    "Select a color for shapes", initialColor);
            Presenter presenter = new Presenter(EducationalSoftGUI.this);
            presenter.setGuiColor(color);
        }
    }

    private class LoadSaveListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String option = e.getActionCommand();
            Presenter presenter = new Presenter(EducationalSoftGUI.this);
            presenter.saveLoadFile(option);
        }
    }

    @Override
    public DrawingArea getDrawingArea() {
        return this.drawingArea;
    }

    @Override
    public String getLineStyle() {
        return Objects.requireNonNull(this.comboStyle.getSelectedItem()).toString();
    }

    @Override
    public String getLineWidth() {
        return this.widthTextField.getText();
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getLineColor() {
        return this.color;
    }

    @Override
    public void setMouseFirstPoint(int x, int y) {
        this.oldX = x;
        this.oldY = y;
    }

    @Override
    public void setNewPoint(int x, int y) {
        this.newX = x;
        this.newY = y;
    }

    @Override
    public int getOldX() {
        return this.oldX;
    }

    @Override
    public int getOldY() {
        return this.oldY;
    }

    @Override
    public int getNewX() {
        return this.newX;
    }

    @Override
    public int getNewY() {
        return this.newY;
    }

    @Override
    public void setCircleRadius(String radius) {
        this.circleRadiusLabel.setText(radius);
    }

    @Override
    public void setCircleLength(String circleLength) {
        this.lengthCircleLabel.setText(circleLength);
    }

    @Override
    public void setCircleAria(String circleAria) {
        this.circleAriaLabel.setText(circleAria);
    }

    @Override
    public void setSectionAria(String sectionAria) {
        this.sectionAriaLabel.setText(sectionAria);
    }

    @Override
    public void setArcLength(String arcLength) {
        this.arcLengthLabel.setText(arcLength);
    }

    @Override
    public String getArcStartAngle() {
        return this.startArcAngle.getText();
    }

    @Override
    public String getArcEndAngle() {
        return this.endArcAngle.getText();
    }

    //for polygon
    @Override
    public String getNbOfVertices() {
        return this.numberOfVerticesText.getText();
    }

    @Override
    public String getTriangleProperty() {
        return Objects.requireNonNull(this.propertiesTriangle.getSelectedItem()).toString();
    }

    @Override
    public List<Float> getTriangleX() {
        return this.triangleX;
    }

    @Override
    public List<Float> getTriangleY() {
        return this.triangleY;
    }

    @Override
    public void setTriangleX(List<Float> x) {
        this.triangleX = x;
    }

    @Override
    public void setTriangleY(List<Float> y) {
        this.triangleY = y;
    }

    @Override
    public String getDrawingOption() {
        return buttonGroupRadio.getSelection().getActionCommand();
    }


}
