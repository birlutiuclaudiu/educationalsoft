package view;

import presenter.Presenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DrawingArea extends JPanel {
    private IEduSoftView iEduSoftView;

    public DrawingArea() {

        MyMouseListener myMouseListener = new MyMouseListener();
        JLabel title = new JLabel("Drawing AREA");
        title.setAlignmentX(60);
        this.add(title);
        this.setPreferredSize(new Dimension(900, 600));
        this.addMouseListener(myMouseListener);
        this.addMouseMotionListener(myMouseListener);
    }

    public void setBound(IEduSoftView iEduSoftView) {
        this.iEduSoftView = iEduSoftView;
    }

    private class MyMouseListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            Presenter presenter = new Presenter(DrawingArea.this.iEduSoftView);
            presenter.setOldPoint(e.getX(), e.getY());
        }

        public void mouseDragged(MouseEvent e) {
            Presenter presenter = new Presenter(DrawingArea.this.iEduSoftView);
            presenter.setNewPoints(e.getX(), e.getY());
            presenter.drawElements();
        }

        public void mouseReleased(MouseEvent e) {
            Presenter presenter = new Presenter(iEduSoftView);
            presenter.drawElements();
        }
    }

}
