package view;
import java.awt.*;
import java.util.List;

public interface IEduSoftView {

    public DrawingArea getDrawingArea();

    public String getLineStyle();

    public String getLineWidth();

    public Color getLineColor();

    public void setColor(Color color);

    //pentru desenarea cu mouse-ul
    public void setMouseFirstPoint(int x, int y);

    public void setNewPoint(int x, int y);

    public int getOldX();

    public int getOldY();

    public int getNewX();

    public int getNewY();

    //circle properties operations
    public void setCircleRadius(String radius);

    public void setCircleLength(String circleLength);

    public void setCircleAria(String circleAria);

    //pentru Arcul de cerc
    public void setSectionAria(String sectionAria);

    public void setArcLength(String arcLength);

    public String getArcStartAngle();

    public String getArcEndAngle();


    public String getDrawingOption();

    //for polygon
    public String getNbOfVertices();

    //for triangle
    public String getTriangleProperty();

    public List<Float> getTriangleX();

    public List<Float> getTriangleY();

    public void setTriangleX(List<Float> x);

    public void setTriangleY(List<Float> y);

}
