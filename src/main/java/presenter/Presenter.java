package presenter;

import model.*;
import model.Point;
import model.Polygon;
import view.DrawingArea;
import view.IEduSoftView;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Presenter {
    private final IEduSoftView iEduSoftView;
    private Drawing drawing;
    private Color color;
    private String lineStyle;
    private float width;
    private static final String PERSISTENCE_FILE = "currentDrawing.xml";

    public Presenter(IEduSoftView iEduSoftView) {
        this.iEduSoftView = iEduSoftView;
    }

    public void drawElements() {
        DrawingArea drawingArea = this.iEduSoftView.getDrawingArea();
        Graphics g = drawingArea.getGraphics();
        drawingArea.paint(g);   //for cleaning the panel
        Graphics2D graphics2D = (Graphics2D) g.create();
        drawing = new Drawing();
        try {
            getLineProperties();
        } catch (NumberFormatException n) {
            this.width = 1.0f;
        }
        switch (this.iEduSoftView.getDrawingOption()) {
            case "circle":
                constructCircle();
                drawing.drawGeometricElements(graphics2D);
                break;
            case "polygon":
                constructPolygon();
                drawing.drawGeometricElements(graphics2D);
                break;
            case "triangle":
                Triangle triangle = constructTriangle();
                switch (this.iEduSoftView.getTriangleProperty()) {
                    case "Circumscribed Circle":
                        drawCircumscribedCircleOfTriangle(triangle);
                        break;
                    case "Inscribed Circle":
                        drawInscribedCircleOfTriangle(triangle);
                        break;
                    case "Tucker Circle":
                        drawingElementsOfTriangle(new TuckerCircleProperty(triangle));
                        break;
                    case "Lucas Circle":
                        drawingElementsOfTriangle(new LucasCircleProperty(triangle));
                        break;
                    case "Orthocentroidal Circle":
                        drawingElementsOfTriangle(new OrthocentroidalCircleProperty(triangle));
                        break;
                    case "Neuberg Circle":
                        drawingElementsOfTriangle(new NeubergCricleProperty(triangle));
                        break;
                    case "Adams Circle":
                        drawingElementsOfTriangle(new AdamsCircleProperty(triangle));
                        break;
                    case "Brocard Circle":
                        drawingElementsOfTriangle(new BrocardCircleProperty(triangle));
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        drawing.drawGeometricElements(graphics2D);
        //for persistence operation
        PersistenceGeometricElementDrawable persistence = new PersistenceGeometricElementDrawable();
        persistence.saveElementToXML(drawing,PERSISTENCE_FILE);
    }


    //for drawing the circle and his specific components
    private void constructCircle() {
        Point center = new Point(this.iEduSoftView.getOldX(), this.iEduSoftView.getOldY());
        Point point2 = new Point(this.iEduSoftView.getNewX(), this.iEduSoftView.getNewY());
        float radius = center.getDistance(point2);
        Circle circle = new Circle(center, radius);
        Line radiusLine = new Line(center, point2);

        drawing.addGeometricElement(new PointDrawable(center, this.color, this.lineStyle, this.width));
        drawing.addGeometricElement(new LineDrawable(radiusLine, this.color, this.lineStyle, this.width));
        drawing.addGeometricElement(new CircleDrawable(circle, this.color, this.lineStyle, this.width));
        //computations to determine the arc properties
        float startAngleArc = 0;
        float endAngelArc = 0;
        try {
            startAngleArc = Integer.parseInt(this.iEduSoftView.getArcStartAngle());
            endAngelArc = Integer.parseInt(this.iEduSoftView.getArcEndAngle());
        } catch (NumberFormatException exc) {
            startAngleArc = 0;
            endAngelArc = 0;
        } finally {
            //the two values must be between 0 and 360
            startAngleArc %= 360;
            startAngleArc %= 360;
        }
        Arc arc = new Arc(startAngleArc, endAngelArc, center, radius);
        //the case in which the arc should not be drawn
        if (!(startAngleArc == 0 && endAngelArc == 0)) {
            Random r = new Random();
            Color randColor = new Color(r.nextFloat(), r.nextFloat() / 2f, r.nextFloat() / 2f);
            drawing.addGeometricElement(new ArcDrawable(arc, randColor, "discontinue", 3.0f));
        }
        setCircleProperties(circle, arc);
    }

    private void getLineProperties() throws NumberFormatException {
        this.color = this.iEduSoftView.getLineColor();
        this.lineStyle = this.iEduSoftView.getLineStyle();
        this.width = Float.parseFloat(this.iEduSoftView.getLineWidth());
    }

    private void setCircleProperties(Circle circle, Arc arc) {
        DecimalFormat df = new DecimalFormat("0.00");
        this.iEduSoftView.setCircleRadius("R: " + df.format(circle.getRadius()));
        this.iEduSoftView.setCircleLength(df.format(circle.computeLength()));
        this.iEduSoftView.setCircleAria(df.format(circle.computeAria()));
        this.iEduSoftView.setArcLength(df.format(arc.getArcLength()));
        this.iEduSoftView.setSectionAria(df.format(arc.getSectorAria()));
    }

    private void constructPolygon() {
        Point center = new Point(this.iEduSoftView.getOldX(), this.iEduSoftView.getOldY());
        Point point2 = new Point(this.iEduSoftView.getNewX(), this.iEduSoftView.getNewY());
        float radius = center.getDistance(point2);
        int nbVertices = 3;
        try {
            nbVertices = Integer.parseInt(this.iEduSoftView.getNbOfVertices());
            if(nbVertices<3)
                nbVertices = 3; //default it remains 3; to define a triangle -> the minimum polygon
        } catch (NumberFormatException exc) {
            nbVertices = 3;
        }
        float angle = (float) (2 * Math.PI / nbVertices);
        List<Point> pointList = new ArrayList<>();
        //adaugare primul punct
        pointList.add(new Point(center.getX() + (int) Math.round(radius), center.getY()));
        for (int i = 1; i < nbVertices; i++) {
            float px = (float) (Math.cos((angle) * i) * radius + center.getX());
            float py = (float) (Math.sin((angle) * i) * radius + center.getY());
            pointList.add(new Point(px, py));
        }
        GeometricElement polygon = new Polygon(pointList);
        drawing.addGeometricElement(new PolygonDrawable((Polygon) polygon, this.color, this.lineStyle, this.width));
        //circumscribed circle
        drawing.addGeometricElement(new CircleDrawable(new Circle(center, radius), Color.RED, "continue", 2.0f));
        float inscribedRadius = center.getDistanceToALine(new Line(pointList.get(0), pointList.get(1)));
        GeometricElement inscribedCircle = new Circle(center, inscribedRadius);
        drawing.addGeometricElement(new CircleDrawable((Circle) inscribedCircle, Color.BLUE, "continue", 2.0f));
        drawing.addGeometricElement(new PointDrawable(center, Color.BLUE, "continue", 1.0f));
    }

    private Triangle constructTriangle() {
        List<Float> x = this.iEduSoftView.getTriangleX();
        List<Float> y = this.iEduSoftView.getTriangleY();
        Point a = new Point(x.get(0), y.get(0), "A");
        Point b = new Point(x.get(1), y.get(1), "B");
        Point c = new Point(x.get(2), y.get(2), "C");
        Point oldMousePoint = new Point(this.iEduSoftView.getOldX(), this.iEduSoftView.getOldY());
        Point newMousePoint = new Point(this.iEduSoftView.getNewX(), this.iEduSoftView.getNewY());
        if (oldMousePoint.getDistance(a) < 10f) {
            a = newMousePoint;
            a.setIdentifier("A");
            x.set(0, a.getX());
            y.set(0, a.getY());
        } else if (oldMousePoint.getDistance(b) < 10f) {
            b = newMousePoint;
            b.setIdentifier("B");
            x.set(1, b.getX());
            y.set(1, b.getY());
        } else if (oldMousePoint.getDistance(c) < 10f) {
            c = newMousePoint;
            c.setIdentifier("C");
            x.set(2, c.getX());
            y.set(2, c.getY());
        }
        this.iEduSoftView.setTriangleX(x);
        this.iEduSoftView.setTriangleY(y);
        this.iEduSoftView.setMouseFirstPoint(this.iEduSoftView.getNewX(), this.iEduSoftView.getNewY());
        //construct the triangle
        Triangle triangle = new Triangle(a, b, c);
        drawing.addGeometricElement(new TriangleDrawable(triangle, this.color, this.lineStyle, this.width));
        return triangle;
    }

    private void drawCircumscribedCircleOfTriangle(Triangle triangle) {
        Circle circle = triangle.getCircumscribedCircle();
        drawing.addGeometricElement(new CircleDrawable(circle, Color.RED, "continue", 2.0f));
        Point ma = circle.getCenter().getProjection(new Line(triangle.getPoints().get(1), triangle.getPoints().get(2)));
        Point mb = circle.getCenter().getProjection(new Line(triangle.getPoints().get(0), triangle.getPoints().get(2)));
        Point mc = circle.getCenter().getProjection(new Line(triangle.getPoints().get(0), triangle.getPoints().get(1)));
        drawing.addGeometricElement(new LineDrawable(new Line(circle.getCenter(), ma),
                Color.BLUE, "discontinue", 2.0f));
        drawing.addGeometricElement(new LineDrawable(new Line(circle.getCenter(), mb),
                Color.BLUE, "discontinue", 2.0f));
        drawing.addGeometricElement(new LineDrawable(new Line(circle.getCenter(), mc),
                Color.BLUE, "discontinue", 2.0f));
    }

    private void drawInscribedCircleOfTriangle(Triangle triangle) {
        Circle circle = triangle.getInscribedCircle();
        Line bisA = new Line(circle.getCenter(), triangle.getPoints().get(0));
        Line bisB = new Line(circle.getCenter(), triangle.getPoints().get(1));
        Line bisC = new Line(circle.getCenter(), triangle.getPoints().get(2));
        drawing.addGeometricElement(new CircleDrawable(circle, Color.green, "discontinue", 2.0f));
        drawing.addGeometricElement(new LineDrawable(bisA, Color.MAGENTA, "discontinue", 2f));
        drawing.addGeometricElement(new LineDrawable(bisB, Color.MAGENTA, "discontinue", 2f));
        drawing.addGeometricElement(new LineDrawable(bisC, Color.MAGENTA, "discontinue", 2f));
    }

    private void drawingElementsOfTriangle(OperationCircleProperties operation) {
        List<GeometricElement> geometricElements = operation.getResultComponents();
        for (GeometricElement geom : geometricElements) {
            if (geom instanceof Point) {
                drawing.addGeometricElement(new PointDrawable((Point) geom, Color.BLUE, "continue", 2.0f));
            } else if (geom instanceof Circle) {
                drawing.addGeometricElement(new CircleDrawable((Circle) geom, Color.RED, "continue", 2.0f));
            } else if (geom instanceof Line) {
                drawing.addGeometricElement(new LineDrawable((Line) geom, Color.MAGENTA, "discontinue", 2.0f));
            }
        }
    }

    ////////////////////////////////////////////// GUI SETTERS ////////////////////////////////////////////////////////////////
    public void setGuiColor(Color color) {
        this.iEduSoftView.setColor(color);
    }

    public void setOldPoint(int x, int y) {
        this.iEduSoftView.setMouseFirstPoint(x, y);
    }

    public void setNewPoints(int x, int y) {
        this.iEduSoftView.setNewPoint(x, y);
    }

    /////////////////////////////////////////////   FOR PERSISTENCE    //////////////////////////////////////////////
    public void saveLoadFile(String option) {
        JFileChooser jFileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jFileChooser.setAcceptAllFileFilterUsed(false);
        jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Only .xml files", "xml"));
        String filePath = "";
        if (option.equals("Save Figure")) {
            int invoke = jFileChooser.showSaveDialog(null);
            if (invoke == JFileChooser.APPROVE_OPTION) {
                filePath = jFileChooser.getSelectedFile().getAbsolutePath();
                saveFigure(filePath);
            }
        } else {
            int invoke = jFileChooser.showOpenDialog(null);
            if (invoke == JFileChooser.APPROVE_OPTION) {
                filePath = jFileChooser.getSelectedFile().getAbsolutePath();
                loadFigure(filePath);
            }
        }
    }
    private void saveFigure(String filePath) {
        PersistenceGeometricElementDrawable persistence = new PersistenceGeometricElementDrawable();
        Drawing drawing1 = new Drawing();
        try {
             drawing1 = persistence.loadElementFromXML(PERSISTENCE_FILE);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "No drawing to save");
            return;
        }
        persistence.saveElementToXML(drawing1, filePath);
    }
    private void loadFigure(String filePath) {
        PersistenceGeometricElementDrawable persistence = new PersistenceGeometricElementDrawable();
        Drawing drawing;
        try {
            drawing = persistence.loadElementFromXML(filePath);
            DrawingArea drawingArea = this.iEduSoftView.getDrawingArea();
            Graphics g = drawingArea.getGraphics();
            drawingArea.paint(g);   //pentru a sterge ecranul
            Graphics2D graphics2D = (Graphics2D) g.create();
            drawing.drawGeometricElements(graphics2D);
            persistence.saveElementToXML(drawing, PERSISTENCE_FILE);
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(null, "Could not load object");
        }

    }

}
